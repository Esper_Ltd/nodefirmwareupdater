import time
import os
import FirmwareSettings
import serial
import CONSTANTS

print("Starting FirmwareUpdater... V3")
print("Tested on Python version 3.10.6")
print()
print("This script assumes that ONE light is connected to the")
print("FIRST dataline of the PROGRAMMING BOX")
print()
print("It will reset that light, assign its address to be 1, and update \n"
      "it to the latest firmware, then quit.")
print()

print("NEW: Before programming, it will attempt to read the stored EEPROM address of a light.")
print("This works with Legacy firmware, Series-4 firmware, not Gen3 Firmware (non-breathing green ID-LED Lights)")

input("Press Enter to continue...")

comPort = serial.Serial()
comPort.port = FirmwareSettings.COM_PORT
comPort.baudrate = 250000
comPort.stopbits = serial.STOPBITS_ONE
comPort.parity = serial.PARITY_NONE
comPort.bytesize = serial.EIGHTBITS
comPort.timeout = 0

LINE = 0


def wait(durationSecs, message):
    print(message + " ", end='')
    for x in range(0, 10):
        time.sleep(durationSecs/10)
        print(".", end='')
    print()


def putLightInBootloadMode(addressToUpdate):
    print("Putting light in bootload mode: " + str(addressToUpdate))
    time.sleep(0.25)
    modBytes = []
    modBytes.append(addressToUpdate)
    modBytes.append(CONSTANTS.FC_WRITE_SINGLE_HOLDING_REGISTER)
    modBytes += splitValueToBytes(CONSTANTS.HR_BOOTLOAD)
    modBytes += [CONSTANTS.BOOTLOAD_KEY_1]
    modBytes += [CONSTANTS.BOOTLOAD_KEY_2]
    reply = sendModMsg(modBytes, 8)
#    verifyCRC(reply)
    print()


def getReply(delayTime=0.080):
    time.sleep(delayTime)
    bytesBack = comPort.readall()
    print(" <<< ", end="")
    for byt in bytesBack:
        print(str(byt) + ", ", end="")
    print()
    listBytes = []
    for byt in bytesBack:
        listBytes.append(int(byt))
    return listBytes


def openSerialPort():
    print("Opening serial port..")
    comPort.open()
    if comPort.isOpen():
        print("Port open...")
    else:
        print("ERR: couldn't open port: " + FirmwareSettings.COM_PORT)
        print("Quitting...")
        exit(345)
    print()


def closeSerialPort():
    print("Closing serial port..")
    comPort.close()
    if not comPort.isOpen():
        print("Port closed...")
    else:
        print("ERR: couldn't close port: " + FirmwareSettings.COM_PORT)
        print("Quitting...")
        exit(345)
    print()


def avrdude(fileName = FirmwareSettings.FILE_NAME_OLD):
    enterProgrammingPassThruMode()
    closeSerialPort()
    time.sleep(0.5)
    avrdudeProcess = os.system('avrdude '
                               '-P' + FirmwareSettings.COM_PORT +
                               ' -c arduino '
                               ' -vv '
                               ' -F '
                               ' -pm328p '
                               ' -b9600 '
                               ' -D '
                               ' -Uflash:w:' + fileName + ':i')
    if avrdudeProcess != 0:
        print("ERR: avrdude reported error, see previous printout.")
        exit(-234)
    else:
        print("SUCC - AVRDUDE REPORTS GOOD UPLOAD")
        print("WAIT FOR CB TO START UP")
    time.sleep(5)
    openSerialPort()
    print()


def verifyCRC(controllerBoxBytes):
    if len(controllerBoxBytes) > 0:
        modCRCBytes = [controllerBoxBytes[len(controllerBoxBytes)-2], controllerBoxBytes[len(controllerBoxBytes)-1]]
        # modBytes = controllerBoxBytes[2:len(controllerBoxBytes) - 2]
        modBytes = controllerBoxBytes[0:len(controllerBoxBytes)-2]
        crc_V = 0xFFFF
        for byt in modBytes:
            crc_V ^= byt & 0xFF
            for i in range(8, 0, -1):
                if crc_V & 0x0001:
                    crc_V >>= 1
                    crc_V ^= 0xA001
                else:
                    crc_V >>= 1
        crc_V_MSB = (crc_V & 0xFF00) >> 8
        crc_V_LSB = (crc_V & 0xFF)
        print("CRC_recd: {" + str(modCRCBytes[0]) + ", " + str(modCRCBytes[1]) + "}")
        print("CRC_calc: {" + str(crc_V_LSB) + ", " + str(crc_V_MSB) + "}")
        if (modCRCBytes[0] == crc_V_LSB) and (modCRCBytes[1] == crc_V_MSB):
            print("CRC good")
        else:
            print("ERR: CRC mismatch... quitting")
            quit(345)


def getFirmwareVersion(address):
    print("Reading light version number for address" + str(address))
    modBytes = []
    modBytes.append(address)
    modBytes.append(CONSTANTS.FC_READ_MULTIPLE_INPUT_REGISTERS)
    modBytes += splitValueToBytes(CONSTANTS.IR_VERSION_NUMBER)
    modBytes += [0]
    modBytes += [1]
    reply = sendModMsg(modBytes, 7)
    verifyCRC(reply)
    if len(reply) == 7:
        versionNumberRead = (reply[3] << 8) + reply[4]
        print("read version number: " + str(versionNumberRead))
        return versionNumberRead
    else:
        print("ERR: couldn't read version number...")
        print("Quitting...")
        quit(100)

def readLightEEPROMStoredAddress(address):
    print("Reading Light EEPROM Stored Address" + str(address))
    modBytes = []
    modBytes.append(address)
    modBytes.append(CONSTANTS.FC_READ_MULTIPLE_HOLDING_REGISTERS)
    modBytes += splitValueToBytes(CONSTANTS.HR_EEPROM_ADDRESS)
    modBytes += [0]
    modBytes += [1]
    reply = sendModMsg(modBytes, 7)
    verifyCRC(reply)
    if len(reply) == 7:
        storedEEP = (reply[3] << 8) + reply[4]
        print("read stored Address: " + str(storedEEP))
        return storedEEP
    else:
        print("ERR: couldn't read stored Address...")
        print("Quitting...")
        quit(100)


def sendBytes(bts):
    comPort.readall()
    comPort.write(bts)
    print(" >>> ", end = "")
    for byte in bts:
        print(str(byte) + " ", end = "")
    print()


def sendControlMessage(messageBytes):
    numericBytes = [97]
    for byt in messageBytes:
        if type(byt) == str:
            numericBytes.append(ord(byt))
        elif type(byt) == int:
            numericBytes.append(byt)
        else:
            print("ERR: type error")
            print("Quitting...")
            exit(789)
    numericBytes.insert(1, len(numericBytes)+1)
    sendBytes(numericBytes)
    time.sleep(0.050)


def splitValueToBytes(value):
    toRet = []
    toRet.append((value & 0x00FF) >> 8)
    toRet.append((value & 0xFF))
    return toRet


def enterProgrammingPassThruMode():
    print("Bypass!")
    # print("entering PPT mode")
    # wait(0.05, "")
    # sendBytes([33, 4, 113, 135])
    # getReply()
    # print()


def setNewBaudRate(nbr):
    comPort.close()
    comPort.baudrate = nbr
    comPort.open()


def appendCRC(message):
    crc = 0xFFFF
    toRet = []
    for byt in message:
        toRet.append(byt)
        crc ^= byt & 0xFF
        for i in range(8, 0, -1):
            if crc & 0x0001:
                crc >>= 1
                crc ^= 0xA001
            else:
                crc >>= 1
    toRet.append(crc & 0xFF)
    toRet.append(crc >> 8)
    return toRet


def sendModMsg(msg, replyLengthExpected=-1):
    if replyLengthExpected == -1:
        replyLengthExpected = len(msg)
    crcMessage = appendCRC(msg)
    # crcMessage = [64] + [-1] + [LINE] + [len(crcMessage)] + [replyLengthExpected] + crcMessage
    # crcMessage[1] = len(crcMessage)
    sendBytes(crcMessage)
    bytesBack = getReply()
    return bytesBack


def testFlashGo():
    print("Flashing all light lines")
    messageBytes = [0, 102]
    sendControlMessage(messageBytes)
    getReply()
    print()


def globalLoadEEPROMAddresses():
    print("Loading EEPROM Addresses...")
    bytes = []
    bytes.append(0)  # address
    bytes.append(CONSTANTS.FC_WRITE_SINGLE_COIL)  # FC
    bytes.append(0)  # MSB COIL ADDR
    bytes.append(CONSTANTS.COIL_SET_ADDRESS_FROM_EEPROM)  # LSB COIL ADR
    bytes.append(255)
    bytes.append(0)
    reply = sendModMsg(bytes)
    verifyCRC(reply)
    print()


def resetLight(addr):
    print("resetting light at address " + str(addr))
    bytes = []
    bytes.append(addr)
    bytes.append(CONSTANTS.FC_WRITE_SINGLE_HOLDING_REGISTER)
    bytes.append(0)
    bytes.append(2)
    bytes.append(0xD7)
    bytes.append(0x39)
    reply = sendModMsg(bytes)
    verifyCRC(reply)
    print()


def pingLight(address, forceContinue = False):
    print("Pinging light: " + str(address))
    modBytes = []
    modBytes.append(address)
    modBytes.append(CONSTANTS.FC_READ_MULTIPLE_INPUT_REGISTERS)
    modBytes += splitValueToBytes(CONSTANTS.IR_LED_COUNT)
    modBytes += [0]
    modBytes += [1]
    reply = sendModMsg(modBytes, 7)
    verifyCRC(reply)
    if len(reply) == 9:
        numLEDs = (reply[5] << 8) + reply[6]
        print("ping - numLEDs: " + str(numLEDs))
        if numLEDs == 3:
            print("Pinged light: " + str(address))
            print()
            return True
    else:
        if not forceContinue:
            print("ERR: couldn't ping light " + str(address) + " quitting ")
            exit(456)
        else:
            return False


def putLightInProgrammingStandbyMode(address):
    print("Entering programming mode (" + str(address) + ")")
    modBytes = []
    modBytes.append(address)
    modBytes.append(CONSTANTS.FC_WRITE_SINGLE_HOLDING_REGISTER)
    modBytes += splitValueToBytes(CONSTANTS.HR_ENTER_PROGRAMMING_MODE)
    modBytes += [CONSTANTS.PROGRAMMING_MODE_KEY_1]
    modBytes += [CONSTANTS.PROGRAMMING_MODE_KEY_2]
    reply = sendModMsg(modBytes)
    verifyCRC(reply)
    if len(reply) != 8:
        print("ERR: reply length incorrect... quitting ")
        quit(9)
    if reply[4] != 49:
        print("ERR: reply verification source error, quitting")
        quit(9)
    print()


def setDirectID(address):
    print("Setting Direct ID")
    modBytes = []
    modBytes.append(0)
    modBytes.append(CONSTANTS.FC_WRITE_SINGLE_HOLDING_REGISTER)
    modBytes += splitValueToBytes(CONSTANTS.HR_DEVICE_ADDRESS)
    modBytes += [(address & 0xFF00) >> 8]
    modBytes += [(address & 0xFF)]
    reply = sendModMsg(modBytes)
    verifyCRC(reply)
    print("Direct ID set.")
    if reply[1] != 6:
        print("ERROR: function code error")
        quit(342)

def setEEPROMID(address, newAddress):
    print("Setting EEPROM ID")
    modBytes = []
    modBytes.append(address)
    modBytes.append(CONSTANTS.FC_WRITE_SINGLE_HOLDING_REGISTER)
    modBytes += splitValueToBytes(CONSTANTS.HR_EEPROM_ADDRESS)
    modBytes += [(newAddress & 0xFF00) >> 8]
    modBytes += [(newAddress & 0xFF)]
    reply = sendModMsg(modBytes)
    verifyCRC(reply)
    if reply[1] != 6:
        print("ERROR: function code error")
        quit(342)
    print()



def setLowLight(address, brightnessValue, led = 1):
    print("Setting Light Value:" + str(brightnessValue) + " addr:" + str(address))

    if brightnessValue <= 200:
        modBytes = []
        modBytes.append(address)
        modBytes.append(CONSTANTS.FC_WRITE_SINGLE_HOLDING_REGISTER)
        modBytes += splitValueToBytes(CONSTANTS.HR_PWM_1 + led)
        modBytes += [(brightnessValue & 0xFF00) >> 8]
        modBytes += [brightnessValue & 0xFF]
        reply = sendModMsg(modBytes)
        verifyCRC(reply)
        print()
    else:
        print("ERR: not setting brightness value at more than 200")


def legidmany(howMany):
    for i in range(0, howMany):
        print("Setting Direct ID")
        modBytes = []
        modBytes.append(0)
        modBytes.append(CONSTANTS.FC_WRITE_SINGLE_HOLDING_REGISTER)
        modBytes += splitValueToBytes(CONSTANTS.HR_DEVICE_ADDRESS)
        modBytes += [(1 & 0xFF00) >> 8]
        modBytes += [(1 & 0xFF)]
        reply = sendModMsg(modBytes)
        verifyCRC(reply)
        if reply[0] != 64:
            print("ERROR: no response")
            quit(345)
        if reply[3] != 6:
            print("ERROR: function code error")
            quit(342)
        if reply[4] + reply[5] != 0:
            print("ERROR: write address error")
            quit(343)
        if reply[2] != 0:
            print("ERROR: resp address error")
            quit(348)

def updateEverything():
    print("Current baud rate: " + str(comPort.baudrate))
    oldFirmwareAddresses = []
    newCounter = 0
    testFlashGo()
    wait(2, "")
    globalLoadEEPROMAddresses()
    wait(1, "reset")
    globalLoadEEPROMAddresses()
    wait(1, "reset")
    for addressToQuery in FirmwareSettings.updateList:
        firmwareVersion = getFirmwareVersion(addressToQuery)
        if firmwareVersion == FirmwareSettings.ORIGINAL_FIRMWARE_VERSION_NUMBER:
            oldFirmwareAddresses.append(addressToQuery)
        elif firmwareVersion == FirmwareSettings.NEW_FIRMWARE_VERSION:
            newCounter += 1
        else:
            print("Error reading firmware version at address: " + str(addressToQuery))
            quit(-10)
        putLightInProgrammingStandbyMode(addressToQuery)
    print()
    print("Found " + str(len(oldFirmwareAddresses)) + " lights needing update...")
    print("Found " + str(newCounter) + " lights already updated...")
    print()
    time.sleep(2)
    for addressToUpdate in oldFirmwareAddresses:
        putLightInProgrammingStandbyMode(addressToUpdate)
        wait(0.05, "")
        putLightInBootloadMode(addressToUpdate)
        wait(0.05, "")
        avrdude()
        globalLoadEEPROMAddresses()
        wait(1, "")
        globalLoadEEPROMAddresses()
        wait(1, "")
        pingLight(addressToUpdate)
        firmwareVersionReadBack = getFirmwareVersion(addressToUpdate)
        if firmwareVersionReadBack != FirmwareSettings.NEW_FIRMWARE_VERSION:
            print("ERR: Node not confirmed correct firmware version...")
            print("quitting...")
            quit(1000)
        print("Light updated... ")
        time.sleep(1)
        putLightInProgrammingStandbyMode(addressToUpdate)


def pingEveryLight():
    for addressToPing in FirmwareSettings.updateList:
        pingLight(addressToPing)


def readIDBitfield(address):
    print("Reading ID Bitfield for address: " + str(address))
    bitFieldVal = -2
    modBytes = []
    modBytes.append(address)
    modBytes.append(CONSTANTS.FC_READ_MULTIPLE_INPUT_REGISTERS)
    modBytes += splitValueToBytes(CONSTANTS.IR_ID_STATUS)
    modBytes += [0]
    modBytes += [1]
    reply = sendModMsg(modBytes, 7)
    verifyCRC(reply)
    if len(reply) == 9:
        bitFieldVal = (reply[5] << 8) + reply[6]
        print("ID Bitfield: " + str(bitFieldVal))
        if bitFieldVal & 0b1:
            print("UPSTREAM: ASSERTED")
        else:
            print("UPSTREAM: NOT ASSERTED")
        if bitFieldVal & 0b10:
            print("DOWNSTREAM DRV: ASSERTED")
        else:
            print("DOWNSTREAM DRV: NOT ASSERTED")
        if bitFieldVal & 0b100:
            print("DOWNSTREAM SNS: DS/N PRESENT")
        else:
            print("DOWNSTREAM SNS: DS/N NOT PRESENT")
    else:
        print("ERR: couldn't read ID_STATUS bitfield ...")
        print("Quitting...")
        quit(100)
    return bitFieldVal


def programModeEverythingExcept(exceptAddress):
    for addressToEnterProgrammingMode in FirmwareSettings.updateList:
        if addressToEnterProgrammingMode != exceptAddress:
            putLightInProgrammingStandbyMode(addressToEnterProgrammingMode)


def obtainFirmware():
    copyProcess = os.system("cp ../MultiFlash_G1/cmake-build-debug/MF_G1.hex ./programmingData")
    if copyProcess != 0:
        print("ERR: couldn't obtain latest file... quitting")
        exit(10)
    else:
        print("Firmware file obtained")


def updateLight(address):
    firmwareVersion = getFirmwareVersion(address)
    if firmwareVersion == FirmwareSettings.ORIGINAL_FIRMWARE_VERSION_NUMBER:
        putLightInProgrammingStandbyMode(address)
    putLightInBootloadMode(address)
    avrdude()


def downdateLight(address):
    firmwareVersion = getFirmwareVersion(address)
    if firmwareVersion == FirmwareSettings.ORIGINAL_FIRMWARE_VERSION_NUMBER:
        putLightInProgrammingStandbyMode(address)
    putLightInBootloadMode(address)
    avrdude(FirmwareSettings.FILE_NAME_OLD)

openSerialPort()


def eepopEverything():
    testFlashGo()
    wait(2, "lights starting")
    resetLight(0)
    wait(0.5, "reset light")
    setLowLight(0, 0, 0)
    setLowLight(0, 0, 1)
    setLowLight(0, 0, 2)
    for adr in FirmwareSettings.updateList:
        setDirectID(adr)
        setLowLight(adr, 100, 0)
        setLowLight(adr, 100, 1)
        setLowLight(adr, 100, 2)
    print("DONE -- EEPOPPED")

def pingEverythingPossible():
    foundLights = []
    for adr in FirmwareSettings.updateList:
        if pingLight(adr, True):  # 2nd True: force continue, otherwise quit on no reply
            foundLights.append(adr)
    for foundAddress in foundLights:
        print("FOUND: " + str(foundAddress))
    print("\n[", end="")
    for foundAddress in foundLights:
        print("" + str(foundAddress) + ", ", end="")
    print("]")
    print("TOTAL:  " + str(len(foundLights)) + " LIGHTS")


def incrementFirmwareVersionEverything():
    for addr in FirmwareSettings.updateList:
        pingLight(addr)
        programModeEverythingExcept(addr)
        updateLight(addr)
        wait(2, "reset")
        testFlashGo()
        wait(2, "restart")
        globalLoadEEPROMAddresses()
        wait(0.3, "load eeprom")
        globalLoadEEPROMAddresses()
        wait(0.3, "load eeprom")


def resetAllAndUpdateSingleLight():
    sleepTime = 0.080
    resetLight(0)
    time.sleep(1)
    setDirectID(1)
    currentAddress = readLightEEPROMStoredAddress(1)
    time.sleep(sleepTime)
    fwv = getFirmwareVersion(1)
    if fwv == 512:
        putLightInProgrammingStandbyMode(1)
    time.sleep(sleepTime)
    putLightInBootloadMode(1)
    time.sleep(sleepTime)
    avrdude(FirmwareSettings.FILE_NAME)
    time.sleep(3)
    setDirectID(1)
    setEEPROMID(1, currentAddress)


resetAllAndUpdateSingleLight()
quit(0)

while 1 == 1:
    args = input(">>>")
    if len(args) > 0:
        args = args.split()
        print("[ ", end = "")
        for arg in args:
            print(arg + ", ", end = "")
        print(" ]")
        print()
        addressArg = 0
        if len(args) > 1:
            addressArg = int(args[1])
        # if args[0] == "f":
        #     testFlashGo()
        if args[0] == "r":               ## TESTED
            resetLight(addressArg)
        if args[0] == "i":               ## TESTED
            globalLoadEEPROMAddresses()
        if args[0] == "lid":                ## TESTED
            setDirectID(addressArg)
        if args[0] == "l":               ## TESTED
            setLowLight(addressArg, 50, 0)
            setLowLight(addressArg, 50, 1)
            setLowLight(addressArg, 50, 2)
        if args[0] == "o":               ## TESTED
            setLowLight(addressArg, 0, 0)
            setLowLight(addressArg, 0, 1)
            setLowLight(addressArg, 0, 2)
        if args[0] == "bl":               ## TESTED
            putLightInBootloadMode(addressArg)
        if args[0] == "pm":               ## TESTED
            putLightInProgrammingStandbyMode(addressArg)
        # if args[0] == "ppt":
        #     enterProgrammingPassThruMode()
        if args[0] == "q":
            print("User requested exit...")
            quit(0)
        # if args[0] == "osp":
        #     openSerialPort()
        # if args[0] == "ue":
        #     updateEverything()
        # if args[0] == "ifve":
        #     incrementFirmwareVersionEverything()
        # if args[0] == "usl":
        #     updateLight(addressArg)
        # if args[0] == "dsl":
        #     downdateLight(addressArg)
        # if args[0] == "of":
        #     obtainFirmware()
        if args[0] == "avr":               ## TESTED
            avrdude()
        # if args[0] == "sns":
        #     readIDBitfield(addressArg)
        # if args[0] == "cs":
        #     closeSerialPort()
        # if args[0] == "pel":
        #     pingEveryLight()
        # if args[0] == "pme":
        #     programModeEverythingExcept(addressArg)
        # if args[0] == "psl":
        #     pingLight(addressArg)
        if args[0] == "fwv":               ## TESTED
            getFirmwareVersion(addressArg)
        # if args[0] == "pep":
        #     pingEverythingPossible()
        # if args[0] == "eepop":
        #     eepopEverything()
        # if args[0] == "legidmany":
        #     legidmany(addressArg)
        # if args[0] == "rafv":
        #     for i in FirmwareSettings.updateList:
        #         vers = getFirmwareVersion(i)
        #         print("ADDR: " + str(i) + ", VERSION" + str(vers) + "")
        if args[0] == "pmsl":
            resetAllAndUpdateSingleLight()
        if args[0] == "st":
            for i in range(0, 300):
                setLowLight(1, 50, 0)
                setLowLight(1, 50, 1)
                setLowLight(1, 50, 2)


