# Firmware Update Utility

This repo contains a script ```NodeFirmwareUpdater.py``` to update a single light connected to the first output of a
ProgrammingBox. To maintain integrity of the update process and avoid interrupted uploads, it is essential that only 
one light is connected to the programming box at one time.
<br> (A ProgrammingBox is a ControllerBox that has been hardwired to be a serial to 485 converter.)

This script is designed to to bring legacy firmware lights (round MultiFlashes that do not show the "train approaching" 
pattern on startup) up to Gen3/Series4 firmware standard.

#### Prerequisites:
* Avrdude installed
* Python 3.8 onwards 
* Make sure the correct COM port / serial device path is set in FirmwareSettings.py
* Python module `pyserial` must be installed / available

### Usage: <br> 
``python3 NodeFirmwareUpdater.py``

### NEW FEATURE:
The EEPROM address is not preserved during the firmware update process. To avoid having to EEPOP all the lights after
upload, the update utility reads the stored EEPROM address and reprograms it after firmware update.
If needed, this functionality can be bypassed by commenting the lines as indicated below:

```python
## LINE 568 
def resetAllAndUpdateSingleLight():
    sleepTime = 0.080
    resetLight(0)
    time.sleep(1)
    setDirectID(1)
    # currentAddress = readLightEEPROMStoredAddress(1) 
    time.sleep(sleepTime)
    fwv = getFirmwareVersion(1)
    if fwv == 512:
        putLightInProgrammingStandbyMode(1)
    time.sleep(sleepTime)
    putLightInBootloadMode(1)
    time.sleep(sleepTime)
    avrdude(FirmwareSettings.FILE_NAME)
    # time.sleep(3)
    # setDirectID(1)
    # setEEPROMID(1, currentAddress)
```



### Known Issues:
* Legacy AVR/Roundlight firmware will only enter the bootloader if the light is already in programming mode 
<br>  (Programming mode is indicated by rapidly blinking signal LEDs)
* Legacy AVR/Roundlight firmware will only enter the bootloader if the ControllerBox is asserting the ID line.
<br>  (Non-legacy lighting firmware does not have this requirement)

### USBTiny - Get Out Of Jail

If the firmware update process is interrupted, the USB tiny can be used to reset the lights back to legacy state, 
after removing the back cover.
The light does not need 48V power during this process - the USBTiny supplies 5V needed to power the microcontroller.

Usage:

```
# cd to /programmingData/ then: 
avrdude -B1 -v -cusbtiny -pm328p -F -Uflash:w:./2017-11-14.lightnode.application.r2.rc4-with1.0Bootloader.hex
```


